package banque;

import static org.junit.Assert.*;

import org.junit.Test;

public class CompteTest {

	@Test
	public void test() {
		Compte A = new Compte(100);
		assertTrue(A.getSolde() > 0);
		A.Affiche();
		
		
		
	}
	
	public void testcredit() {
		Compte A = new Compte(100);
		assertTrue(A.credit(10) > 0);
	}
	
	public void testdebit() {
		Compte A = new Compte(100);
		assertTrue(A.debit(10) > 0);
	}
	
	public void testdebit2() {
		Compte A = new Compte(100);
		assertTrue(A.debit(10) < A.getSolde());
	}

}
