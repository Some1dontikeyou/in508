package fr.uvsq.ComptesurMaven;

public class Compte {
	private double solde;
	public double valeur;
	
	public Compte(double solde) {
		this.solde = solde;
	}
	
	public void Affiche() {
		System.out.println("Le solde est " + this.solde);
	}
	
	public double getSolde() {
		return this.solde;
	}
	
	public double credit(double valeur) {
		this.solde = solde + valeur;
		return valeur;
	}
	
	public double debit(double valeur) {
		this.solde = solde - valeur;
		return valeur;
	}
	
	
}
